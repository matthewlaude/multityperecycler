package com.example.a3typerecycler.model

import com.example.a3typerecycler.model.remote.TypeApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object TypeRepo {

    private val TypeApi = object : TypeApi {
        override suspend fun getTypes(): List<String> {
            return listOf(
                "Strings", "Integers", "Doubles", "Booleans"
            )
        }

        override suspend fun getStrings(): List<String> {
            return listOf(
                "Apple",
                "Orange",
                "Banana",
                "Cherry",
                "Pineapple",
                "Grape",
                "Blueberry",
                "Coconut",
                "Mango",
                "Kiwi",
                "Raspberry",
                "Jackfruit",
                "Cocoa"
            )
        }
        override suspend fun getInts(): List<Int> {
            return listOf(
                12, 13, 25, 502, 77, -9, 100, -15, 1093, 6, 60, 42, 1234, -501
            )
        }

        override suspend fun getDoubles(): List<Double> {
            return listOf(
                53.27, 65.67, 66.47, 92.97, 87.44, 40.67, 51.45, 22.33, 84.52, 57.23, 11.11
            )
        }

        override suspend fun getBools(): List<Boolean> {
            return listOf(
                true,
                false,
                false,
                true,
                false,
                true,
                true,
                true,
                false,
                false,
                true,
            )
        }

    }

    suspend fun getType(): List<String> = withContext(Dispatchers.IO) {
        TypeApi.getTypes()
    }

    suspend fun getString(): List<String> = withContext(Dispatchers.IO) {
        TypeApi.getStrings()
    }

    suspend fun getInt(): List<Int> = withContext(Dispatchers.IO) {
        TypeApi.getInts()
    }

    suspend fun getDouble(): List<Double> = withContext(Dispatchers.IO) {
        TypeApi.getDoubles()
    }

    suspend fun getBool(): List<Boolean> = withContext(Dispatchers.IO) {
        TypeApi.getBools()
    }
}