package com.example.a3typerecycler.model.remote

interface TypeApi {
    suspend fun getTypes(): List<String>
    suspend fun getStrings(): List<String>
    suspend fun getInts(): List<Int>
    suspend fun getDoubles(): List<Double>
    suspend fun getBools(): List<Boolean>
//    suspend fun getChars(): List<Char>
}