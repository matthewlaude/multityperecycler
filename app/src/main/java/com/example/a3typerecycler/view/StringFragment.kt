package com.example.a3typerecycler.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.a3typerecycler.R
import com.example.a3typerecycler.adapters.StringAdapter
import com.example.a3typerecycler.adapters.TypeAdapter
import com.example.a3typerecycler.databinding.FragmentFirstBinding
import com.example.a3typerecycler.databinding.FragmentStringBinding
import com.example.a3typerecycler.viewmodel.TypeViewModel

class StringFragment : Fragment() {

    private var _binding: FragmentStringBinding? = null
    private val binding get() = _binding!!
    private val typeViewModel by viewModels<TypeViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentStringBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        val args: StringFragmentArgs by navArgs()
//        val item = args.string

        with (binding.rvStrings) {
            layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            adapter = StringAdapter().apply {
                with(typeViewModel) {
                    getString()
                    string.observe(viewLifecycleOwner) {
                        addStringItem(it)
                    }
                }
            }
        }
    }

}