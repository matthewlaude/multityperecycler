package com.example.a3typerecycler.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.a3typerecycler.R
import com.example.a3typerecycler.adapters.IntAdapter
import com.example.a3typerecycler.databinding.FragmentIntBinding
import com.example.a3typerecycler.viewmodel.TypeViewModel

class IntFragment : Fragment() {

    private var _binding: FragmentIntBinding? = null
    private val binding get() = _binding!!
    private val typeViewModel by viewModels<TypeViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentIntBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding.rvInts) {
            layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            adapter = IntAdapter().apply {
                with(typeViewModel) {
                    getInt()
                    int.observe(viewLifecycleOwner) {
                        addIntItem(it)
                    }
                }
            }
        }
    }

}