package com.example.a3typerecycler.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.a3typerecycler.databinding.ItemTypeBinding
import com.example.a3typerecycler.view.FirstFragmentDirections

class TypeAdapter : RecyclerView.Adapter<TypeAdapter.TypeViewHolder>() {

    private var type = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TypeViewHolder {
        val binding = ItemTypeBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return TypeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TypeViewHolder, position: Int) {
        val typeItem = type[position]
        holder.loadFirst(typeItem)
    }

    override fun getItemCount(): Int {
        return type.size
    }

    fun addFirstItem(type: List<String>) {
        this.type = type.toMutableList()
        notifyDataSetChanged()
    }

    class TypeViewHolder(
        private val binding: ItemTypeBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadFirst(typeItem: String) {
            with(binding) {
                tvItemType.text = typeItem

                if (typeItem == "Strings") {
                    tvItemType.setOnClickListener {
                        it.findNavController()
                            .navigate(FirstFragmentDirections.actionFirstFragmentToStringFragment())
                    }
                } else if (typeItem == "Integers") {
                    tvItemType.setOnClickListener {
                        it.findNavController()
                            .navigate(FirstFragmentDirections.actionFirstFragmentToIntFragment())
                    }
                } else if (typeItem == "Doubles") {
                    tvItemType.setOnClickListener {
                        it.findNavController()
                            .navigate(FirstFragmentDirections.actionFirstFragmentToDoubleFragment())
                    }
                } else {
                    tvItemType.setOnClickListener {
                        it.findNavController()
                            .navigate((FirstFragmentDirections.actionFirstFragmentToBoolFragment()))
                    }
                }
            }
        }
    }
}