package com.example.a3typerecycler.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a3typerecycler.databinding.ItemIntBinding

class IntAdapter : RecyclerView.Adapter<IntAdapter.IntViewHolder>() {

    private var int = mutableListOf<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IntAdapter.IntViewHolder {
        val binding = ItemIntBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return IntViewHolder(binding)
    }

    override fun onBindViewHolder(holder: IntAdapter.IntViewHolder, position: Int) {
        val intItem = int[position]
        holder.loadInt(intItem)
    }

    override fun getItemCount(): Int {
        return int.size
    }

    fun addIntItem(int: List<Int>) {
        this.int = int.toMutableList()
        notifyDataSetChanged()
    }

    class IntViewHolder(
        private val binding: ItemIntBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadInt(intItem: Int) {
            with(binding) {
                tvItemInt.text = intItem.toString()
            }
        }
    }
}