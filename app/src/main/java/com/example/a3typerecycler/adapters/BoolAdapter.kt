package com.example.a3typerecycler.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a3typerecycler.databinding.ItemBoolBinding

class BoolAdapter : RecyclerView.Adapter<BoolAdapter.BoolViewHolder>() {

    private var bool = mutableListOf<Boolean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BoolViewHolder {
        val binding = ItemBoolBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return BoolViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BoolViewHolder, position: Int) {
        val boolItem = bool[position]
        holder.loadBool(boolItem)
    }

    override fun getItemCount(): Int {
        return bool.size
    }

    fun addBoolItem(bool: List<Boolean>) {
        this.bool = bool.toMutableList()
        notifyDataSetChanged()
    }

    class BoolViewHolder(
        private val binding: ItemBoolBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadBool(boolItem: Boolean) {
            with(binding) {
                tvItemBool.text = boolItem.toString()
            }
        }
    }

}