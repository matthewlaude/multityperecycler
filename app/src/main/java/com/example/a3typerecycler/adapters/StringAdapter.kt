package com.example.a3typerecycler.adapters

import android.text.Layout
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a3typerecycler.databinding.ItemStringBinding
import com.example.a3typerecycler.databinding.ItemTypeBinding

class StringAdapter : RecyclerView.Adapter<StringAdapter.StringViewHolder>() {

    private var string = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StringViewHolder {
        val binding = ItemStringBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return StringViewHolder(binding)
    }

    override fun onBindViewHolder(holder: StringViewHolder, position: Int) {
        val stringItem = string[position]
        holder.loadString(stringItem)
    }

    override fun getItemCount(): Int {
        return string.size
    }

    fun addStringItem(string: List<String>) {
        this.string = string.toMutableList()
        notifyDataSetChanged()
    }

    class StringViewHolder(
        private val binding: ItemStringBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadString(stringItem: String) {
            with(binding) {
                tvItemString.text = stringItem
            }
        }
    }
}