package com.example.a3typerecycler.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a3typerecycler.databinding.ItemDoubleBinding

class DoubleAdapter : RecyclerView.Adapter<DoubleAdapter.DoubleViewHolder>() {

    private var double = mutableListOf<Double>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DoubleViewHolder {
        val binding = ItemDoubleBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return DoubleViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DoubleViewHolder, position: Int) {
        val doubleItem = double[position]
        holder.loadDouble(doubleItem)
    }

    override fun getItemCount(): Int {
        return double.size
    }

    fun addDoubleItem(double: List<Double>) {
        this.double = double.toMutableList()
        notifyDataSetChanged()
    }

    class DoubleViewHolder(
        private val binding: ItemDoubleBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadDouble(doubleItem: Double) {
            with(binding) {
                tvItemDouble.text = doubleItem.toString()
            }
        }
    }
}