package com.example.a3typerecycler.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a3typerecycler.model.TypeRepo
import kotlinx.coroutines.launch

class TypeViewModel : ViewModel() {

    private val repo = TypeRepo

    private val _type = MutableLiveData<List<String>>()
    val type: LiveData<List<String>> get() = _type

    private val _string = MutableLiveData<List<String>>()
    val string: LiveData<List<String>> get() = _string

    private val _int = MutableLiveData<List<Int>>()
    val int: LiveData<List<Int>> get() = _int

    private val _double = MutableLiveData<List<Double>>()
    val double: LiveData<List<Double>> get() = _double

    private val _bool = MutableLiveData<List<Boolean>>()
    val bool: LiveData<List<Boolean>> get() = _bool


    fun getType() {
        viewModelScope.launch {
            val itemType = repo.getType()
            _type.value = itemType
        }
    }
    fun getString() {
        viewModelScope.launch {
            val itemString = repo.getString()
            _string.value = itemString
        }
    }
    fun getInt() {
        viewModelScope.launch {
            val itemInt = repo.getInt()
            _int.value = itemInt
        }
    }
    fun getDouble() {
        viewModelScope.launch {
            val itemDouble = repo.getDouble()
            _double.value = itemDouble
        }
    }
    fun getBool() {
        viewModelScope.launch {
            val itemBool = repo.getBool()
            _bool.value = itemBool
        }
    }
}